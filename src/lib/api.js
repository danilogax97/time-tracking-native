import axios from 'axios';
import qs from 'qs';

const genericHeader = {
	'content-type': 'application/x-www-form-urlencoded',
	'Access-Control-Allow-Origin': '*',
};
const genericConfig = {
	crossDomain: true,
};
const makeUrl = (path) => `http://192.168.0.35:8000/v1${path}`;
export default class Api {
	static async signUp(data) {
		return axios({
			method: 'post',
			url: makeUrl('/sign-up'),
			headers: {
				...genericHeader,
				Authorization: 'Basic application',
			},
			data: qs.stringify({ ...data, clientId: 'application', grant_type: 'password' }),
			...genericConfig,
		});
	}

	static async signIn(data) {
		return axios({
			method: 'post',
			url: makeUrl('/sign-in'),
			headers: {
				...genericHeader,
				Authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
				ContentType: 'application/x-www-form-urlencoded',
			},
			data: qs.stringify({
				...data,
				grant_type: 'password',
			}),
			...genericConfig,
		});
	}

	static async refreshToken(token) {
		return axios({
			method: 'post',
			url: makeUrl('/sign-in'),
			headers: {
				...genericHeader,
				Authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
			},
			data: qs.stringify({
				refresh_token: token,
				grant_type: 'refresh_token',
			}),
			...genericConfig,
		});
	}

	static async postActivity(token, startTime, endTime) {
		return axios({
			method: 'post',
			url: makeUrl('/add-activity'),
			headers: {
				...genericHeader,
				Authorization: `Bearer ${token}`,
			},
			data: qs.stringify({
				startTime: startTime.format('YYYY-MM-DD HH:mm:ss'),
				endTime: endTime.format('YYYY-MM-DD HH:mm:ss'),
			}),
			...genericConfig,
		});
	}

	static async getReports(token) {
		return axios({
			method: 'get',
			url: makeUrl('/reports'),
			headers: {
				...genericHeader,
				Authorization: `Bearer ${token}`,
			},
			...genericConfig,
		});
	}

	static async getLastActivity(token) {
		return axios({
			method: 'get',
			url: makeUrl('/last-activity'),
			headers: {
				...genericHeader,
				Authorization: `Bearer ${token}`,
			},
			...genericConfig,
		});
	}

	static async signOut(token) {
		return axios({
			method: 'delete',
			url: makeUrl('/sign-out'),
			headers: {
				...genericHeader,
				Authorization: `Bearer ${token}`,
			},
			...genericConfig,
		});
	}
}
