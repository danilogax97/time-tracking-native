import dayjs from "dayjs";

export const displayDifference = (startTime, endTime) => (
    dayjs(endTime).subtract(startTime.hour(), 'hour').subtract(startTime.minute(), 'minute').subtract(startTime.second(), 'second')
        .format('HH[h] mm[m]')
);
