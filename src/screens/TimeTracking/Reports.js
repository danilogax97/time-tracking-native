import React from 'react';
import { ActivityIndicator, AsyncStorage, Text } from 'react-native';
import { connect } from 'react-redux';
import withMainAppLayout from '../../Components/Layouts/MainAppLayout';
import { CurrentDateDisplay } from '../../Components/CurrentDateDisplay';
import { HeaderText } from '../../Components/Text';
import { fetchReports } from '../../store/reports/ReportsActions';
import { Report } from '../../Components/Report';
import { ScrollViewWrapper } from '../../Components/Wrappers';


class Reports extends React.Component {
	async componentDidMount() {
		const {
			getReports,
		} = this.props;
		await getReports(await AsyncStorage.getItem('accessToken'));
	}

	render() {
		const {
			reports,
			loading,
			error,
		} = this.props;
		if (loading) {
			return (
				<ScrollViewWrapper>
					<ActivityIndicator />
				</ScrollViewWrapper>
			);
		}
		if (error) {
			return (
				<ScrollViewWrapper>
					<Text>Something wrong happened</Text>
				</ScrollViewWrapper>
			);
		}
		return (
			<ScrollViewWrapper>
				<CurrentDateDisplay />
				<HeaderText>
					Reports
				</HeaderText>
				{Object.keys(reports).map((el) => (
					<Report
						key={el}
						time={reports[el].reports[0].startTime}
						activities={reports[el].reports}
						total={reports[el].total}
					/>
				))}
			</ScrollViewWrapper>
		);
	}
}
const mapStateToProps = (state) => ({
	reports: state.getIn(['reportsReducer', 'reports']).toJS(),
	loading: state.getIn(['reportsReducer', 'loading']),
	error: state.getIn(['reportsReducer', 'error']),
});
const mapDispatchToProps = (dispatch) => ({
	getReports: (token) => dispatch(fetchReports(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withMainAppLayout(Reports));
