import React from 'react';
import { AsyncStorage } from 'react-native';
import styled from 'styled-components';
import dayjs from 'dayjs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withMainAppLayout from '../../Components/Layouts/MainAppLayout';
import { Button } from '../../Components/Button';
import Api from '../../lib/api';
import {
	setError, startTimer, stopTimer, tick,
} from '../../store/timer/TimerActions';
import { CurrentDateDisplay } from '../../Components/CurrentDateDisplay';
import { fetchReports } from '../../store/reports/ReportsActions';
import {
	Bold, GrayText, RedText, TextLarger,
} from '../../Components/Text';
import { Timer } from '../../Components/Timer';

const ContentWrapper = styled.View`
	padding-left: 20;
	padding-right: 20;
	margin-top: 5%;
	flex:16;
	align-self: stretch;
`;
const Header = styled.View`

`;

function UnderButtonInstruction({ running }) {
	return (
		<>
			{!running ? (
				<GrayText>
				Clicking
					<Bold> ClockIn </Bold>
				will start the timer
				</GrayText>
			) : (
				<RedText>
				Clicking
					<Bold> ClockOut </Bold>
				will stop the timer
				</RedText>
			)}
		</>
	);
}


class Dashboard extends React.Component {
	async componentDidMount() {
		const {
			getReports,
		} = this.props;
		await getReports(await AsyncStorage.getItem('accessToken', () => {}));
	}

	componentWillUnmount() {
		const {
			timerId,
		} = this.props;
		clearInterval(timerId);
	}

	getTodayTotal() {
		const {
			reports,
		} = this.props;
		try {
			const [day, data] = Object.entries(reports)[0];
			if (dayjs().format('DD MMMM') === day) {
				return data.total.format('HH:mm');
			}
		} catch {
			return '00:00';
		}
		return '00:00';
	}

	async handleClick() {
		const stopTime = dayjs();
		const {
			actions, running, startTime, timerId,
		} = this.props;

		if (!running) {
			const interval = setInterval(() => {
				actions.tick();
			}, 1000);
			actions.startTimer(interval);
		} else {
			clearInterval(timerId);
			actions.stopTimer();
			const token = await AsyncStorage.getItem('accessToken', () => {});
			try {
				await Api.postActivity(token, startTime, stopTime);
			} catch (error) {
				actions.setError(error);
			}
		}
	}

	render() {
		const {
			running,
			currentTime,
			navigation,
		} = this.props;

		return (
			<ContentWrapper>
				<CurrentDateDisplay />
				<Header>
					<TextLarger>
						TIME SPENT TODAY:
						{' '}
						{' '}
						<Bold>
							{this.getTodayTotal()}
						</Bold>
					</TextLarger>
				</Header>
				<Timer time={currentTime} />
				<Button
					onPress={() => navigation.push('Reports')}
					text="REPORTS"
					color="#e7e7e7"
					textColor="black"
				/>
				<Button
					onPress={() => this.handleClick()}
					text={running ? 'CLOCK OUT' : 'CLOCK IN'}
					color={running ? '#e96466' : '#20c284'}
					textColor="white"
				/>
				<UnderButtonInstruction running={running} />
			</ContentWrapper>
		);
	}
}
const mapStateToProps = (state) => ({
	running: state.get('timerReducer').get('running'),
	currentTime: state.get('timerReducer').get('currentTime'),
	startTime: state.get('timerReducer').get('startTime'),
	timerId: state.getIn(['timerReducer', 'timerId']),
	reports: state.getIn(['reportsReducer', 'reports']).toJS(),
});
const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators({
		startTimer,
		stopTimer,
		tick,
		setError,
		fetchReports,
	}, dispatch),
	getReports: (token) => dispatch(fetchReports(token)),
});
export default connect(mapStateToProps, mapDispatchToProps)(withMainAppLayout(Dashboard));
