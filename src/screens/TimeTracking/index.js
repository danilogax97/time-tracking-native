import { createStackNavigator } from 'react-navigation-stack';
import Dashboard from './Dashboard';
import Reports from './Reports';

export default createStackNavigator({
	Dashboard: {
		screen: Dashboard,
		path: '/dashboard',

	},
	Reports: {
		screen: Reports,
		path: '/reports',

	},

}, {
	initialRouteName: 'Dashboard',
	headerMode: 'none',
});
