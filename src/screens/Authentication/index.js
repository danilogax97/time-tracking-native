import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import SignUp from './SignUp';
import SignIn from './SignIn';

export default createStackNavigator({
	SignUp: {
		screen: SignUp,
		path: '/sign-up',
		// navigationOptions: ()=>();
	},
	SignIn: {
		screen: SignIn,
		path: 'sign-in',
	},

}, {
	initialRouteName: 'SignIn',
	headerMode: 'none',
});
