import React from 'react';
import {
	ActivityIndicator,
	AsyncStorage,
	Platform,
} from 'react-native';
import withAuthenticationLayout from '../../Components/Layouts/AuthenticationLayout';
import { StyledInput, Label } from '../../Components/Input';
import { FormWrapper, RelativeWrapper } from '../../Components/Wrappers';
import { Button } from '../../Components/Button';
import { LinkTo } from '../../Components/Link';
import Api from '../../lib/api';
import { WhiteText, ErrorMessage } from '../../Components/Text';
import { ShowPassword } from '../../Components/Touchables';
import { TermsAndConditions } from '../../Components/TermsAndConditions';


class SignUp extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			error: false,
			loading: false,
			hidePassword: true,
			email: '',
			name: '',
			password: '',
			emailFocused: false,
			passwordFocused: false,
			nameFocused: false,
		};
	}

	handleSubmit() {
		const {
			name,
			email,
			password,
		} = this.state;

		this.setState({
			loading: true, error: false, emailFocused: false, passwordFocused: false, nameFocused: false,
		}, () => {
			Api.signUp({ fullName: name, email, password })
				.then(() => {
					Api.signIn({ username: email, password })
						.then(async (response) => {
							await AsyncStorage.setItem('accessToken', response.data.accessToken.accessToken, () => {});
							await AsyncStorage.setItem('refreshToken', response.data.accessToken.refreshToken, () => {});
							this.props.navigation.navigate('Dashboard');
						}).catch(() => this.setState((prevState) => ({ ...prevState, loading: false, error: true })));
				}).catch(() => this.setState((prevState) => ({ ...prevState, loading: false, error: true })));
		});
	}

	render() {
		const {
			navigation: { navigate },
		} = this.props;
		const {
			loading,
			error,
			hidePassword,
			password,
			nameFocused,
			emailFocused,
			passwordFocused,
		} = this.state;
		if (loading) {
			return (
				<FormWrapper>
					<ActivityIndicator
						size="large"
						color="#0000ff"
						enabled
					/>
				</FormWrapper>
			);
		}
		return (
			<>
				{error && <ErrorMessage>Some kind of error</ErrorMessage>}
				<FormWrapper>
					<Label focused={nameFocused}>FULL NAME</Label>
					<StyledInput
						placeholder={nameFocused ? '' : 'John Doe'}
						placeholderTextColor="gray"
						onSubmitEditing={() => { this.emailInput.focus(); }}
						onChangeText={(event) => { this.setState({ name: event }); }}
						onFocus={() => this.setState({ nameFocused: true })}
						onBlur={() => Platform.OS === 'ios' && this.setState({ nameFocused: false })}
						focused={nameFocused}
					/>
					<Label focused={emailFocused}>EMAIL</Label>
					<StyledInput
						placeholder={emailFocused ? '' : 'email@example.com'}
						placeholderTextColor="gray"
						ref={(input) => { this.emailInput = input; }}
						onSubmitEditing={() => { this.passwordInput.focus(); }}
						onChangeText={(event) => { this.setState({ email: event }); }}
						onFocus={() => this.setState({ emailFocused: true })}
						onBlur={() => Platform.OS === 'ios' && this.setState({ emailFocused: false })}
						focused={emailFocused}
					/>
					<Label focused={passwordFocused}>PASSWORD</Label>
					<RelativeWrapper>
						<StyledInput
							ref={(input) => { this.passwordInput = input; }}
							onSubmitEditing={() => this.handleSubmit()}
							onChangeText={(event) => { this.setState({ password: event }); }}
							placeholder={passwordFocused ? '' : '5+ characters'}
							placeholderTextColor="gray"
							secureTextEntry={hidePassword}
							onFocus={() => this.setState({ passwordFocused: true })}
							onBlur={() => Platform.OS === 'ios' && this.setState({ passwordFocused: false })}
							focused={passwordFocused}
						/>
						{password.length > 0 && (
							<ShowPassword onPress={() => this.setState((prevState) => ({ hidePassword: !prevState.hidePassword }))}>
								<WhiteText>
								SHOW
								</WhiteText>
							</ShowPassword>
						)}
					</RelativeWrapper>
					<Button onPress={() => this.handleSubmit()} text="SIGN UP" />
					<LinkTo navigateTo={() => navigate('SignIn')} text="Already a member?" linkText="SIGN IN" />
					<TermsAndConditions />
				</FormWrapper>

			</>
		);
	}
}

export default withAuthenticationLayout(SignUp);
