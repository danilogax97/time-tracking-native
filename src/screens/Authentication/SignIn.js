import React from 'react';
import { ActivityIndicator, AsyncStorage, View, Platform } from 'react-native';
import withAuthenticationLayout from '../../Components/Layouts/AuthenticationLayout';
import { FormWrapper } from '../../Components/Wrappers';
import { StyledInput, Label } from '../../Components/Input';
import { Button } from '../../Components/Button';
import { LinkTo } from '../../Components/Link';
import Api from '../../lib/api';
import { WhiteText, ErrorMessage } from '../../Components/Text';
import { ShowPassword } from '../../Components/Touchables';

class SignIn extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			error: false,
			loading: false,
			hidePassword: true,
			email: '',
			password: '',
			emailFocused: false,
			passwordFocused: false,
		};
	}

	handleSubmit() {
		const {
			email,
			password,
		} = this.state;
		const {
			navigation: { navigate },
		} = this.props;

		this.setState({
			loading: true,
			error: false,
			emailFocused: false,
			passwordFocused: false,
		}, () => {
			Api.signIn({
				username: email,
				password,
			}).then(async (response) => {
				await AsyncStorage.setItem('accessToken', response.data.accessToken.accessToken, () => {});
				await AsyncStorage.setItem('refreshToken', response.data.accessToken.refreshToken, () => {});
				navigate('Dashboard');
			}).catch(() => this.setState((prevState) => ({
				...prevState,
				loading: false,
				error: true,
			})));
		});
	}

	render() {
		const {
			loading,
			error,
			hidePassword,
			password,
			email,
			emailFocused,
			passwordFocused,
		} = this.state;
		const {
			navigation: { navigate },
		} = this.props;
		if (loading) {
			return (
				<FormWrapper>
					<ActivityIndicator
						size="large"
						color="#0000ff"
						enabled
					/>
				</FormWrapper>
			);
		}
		return (
			<>
				{error && <ErrorMessage>Email or password are incorrect</ErrorMessage>}
				<FormWrapper>
					<Label focused={emailFocused}>EMAIL</Label>
					<StyledInput
						placeholder={emailFocused ? '' : 'email@example.com'}
						placeholderTextColor="gray"
						selectionColor="white"
						onChangeText={(event) => { this.setState({ email: event }); }}
						onSubmitEditing={() => {
							this.passwordInput.focus();
						}}
						onFocus={() => this.setState({ emailFocused: true })}
						onBlur={() => Platform.OS === 'ios' && this.setState({ emailFocused: false })}
						focused={emailFocused}
					/>
					<Label focused={passwordFocused}>PASSWORD</Label>
					<View>
						<StyledInput
							ref={(input) => {
								this.passwordInput = input;
							}}
							onChangeText={(event) => {
								this.setState({ password: event });
							}}
							onSubmitEditing={() => this.handleSubmit()}
							placeholder={passwordFocused ? '' : '5+ characters'}
							placeholderTextColor="gray"
							selectionColor="white"
							secureTextEntry={hidePassword}
							onFocus={() => this.setState({ passwordFocused: true })}
							onBlur={() => Platform.OS === 'ios' && this.setState({ passwordFocused: false })}
							focused={passwordFocused}
						/>
						{password.length > 0 && (
							<ShowPassword onPress={() => this.setState((prevState) => ({ hidePassword: !prevState.hidePassword }))}>
								<WhiteText>
									{hidePassword ? 'SHOW' : 'HIDE'}
								</WhiteText>
							</ShowPassword>
						)}
					</View>
					<Button onPress={() => this.handleSubmit()} text="SIGN IN" />
					<LinkTo navigateTo={() => navigate('SignUp')} text="Not a member?" linkText="SIGN UP" />
				</FormWrapper>
			</>
		);
	}
}


export default withAuthenticationLayout(SignIn);
