import React from 'react';
import {
	ActivityIndicator, AsyncStorage
} from 'react-native';
import withAuthenticationLayout from '../Components/Layouts/AuthenticationLayout';
import Api from '../lib/api';
import { FormWrapper } from '../Components/Wrappers';

const isTokenValid = async (token) => {
	try {
		await Api.getReports(token);
		return true;
	} catch (error) {
		return false;
	}
};
class Loader extends React.Component {
	async componentDidMount() {
		// artificial wait time
		// await new Promise((resolve, reject) => setTimeout(() => resolve(), 3000));
		const {
			navigation: { navigate },
		} = this.props;
		const token = await AsyncStorage.getItem('accessToken', () => {});
		if (token && await isTokenValid(token)) {
			navigate('Dashboard');
			return;
		}
		const refreshToken = await AsyncStorage.getItem('refreshToken', () => {});
		if (refreshToken) {
			try {
				const {
					data: {
						accessToken: {
							accessToken: newAccessToken,
							refreshToken: newRefreshToken,
						},
					},
				} = await Api.refreshToken(refreshToken);

				await AsyncStorage.setItem('accessToken', newAccessToken, () => {});
				await AsyncStorage.setItem('refreshToken', newRefreshToken, () => {});

				navigate('Dashboard');
			} catch (error) {
				await AsyncStorage.removeItem('accessToken', () => {});
				await AsyncStorage.removeItem('refreshToken', () => {});
				navigate('Auth');
			}
		} else navigate('Auth');
	}

	render() {
		return (
			<FormWrapper>
				<ActivityIndicator
					size="large"
					color="#0000ff"
					enabled
				/>
			</FormWrapper>
		);
	}
}
export default (withAuthenticationLayout(Loader));
