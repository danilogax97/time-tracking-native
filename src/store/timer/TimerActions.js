import Types from './TimerTypes';

export function startTimer(intervalId) {
	return {
		timerId: intervalId,
		type: Types.START_TIMER,
	};
}
export function stopTimer() {
	return {
		type: Types.STOP_TIMER,
	};
}
export function tick() {
	return {
		type: Types.TICK,
	};
}
export function setError(payload) {
	return {
		type: Types.SET_ERROR,
		error: payload,
	};
}
