import {
	fromJS,
} from 'immutable';
import dayjs from 'dayjs';
import Types from './TimerTypes';

const createStartTime = () => dayjs().set('hour', 0).set('minute', 0).set('second', 0);

const initialState = fromJS({
	currentTime: createStartTime(),
	startTime: undefined,
	timerId: undefined,
	running: false,
	error: undefined,
});

export default (state = initialState, action) => {
	switch (action.type) {
	case Types.START_TIMER: {
		return state.set('startTime', dayjs())
			.set('timerId', action.timerId)
			.set('running', true);
	}
	case Types.STOP_TIMER: {
		return state.set('running', false)
			.set('timerId', undefined);
	}
	case Types.TICK: {
		const start = state.get('startTime');
		const newTime = dayjs().subtract(start.hour(), 'hour')
			.subtract(start.minute(), 'minute')
			.subtract(start.second(), 'second');
		return state.set('currentTime', newTime);
	}
	case Types.SET_ERROR: {
		return state.set('error', action.payload);
	}
	case Types.CLEAR_TIMER: {
		return initialState;
	}
	default: return state;
	}
};
