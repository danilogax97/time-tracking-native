import dayjs from 'dayjs';
import customParser from 'dayjs/plugin/advancedFormat';
import Api from '../../lib/api';
import Types from './ReportsTypes';

dayjs.extend(customParser);

export function fetch() {
	return {
		type: Types.FETCH_REPORTS,
	};
}
export function fetchSuccess(reports) {
	return {
		type: Types.FETCH_REPORTS_SUCCESS,
		reports,
	};
}
export function fetchFail(error) {
	return {
		type: Types.FETCH_REPORTS_FAIL,
		error,
	};
}
export function fetchReports(token) {
	return async (dispatch) => {
		dispatch(fetch());
		try {
			const response = await Api.getReports(token);

			const reports = {};
			response.data.reports.forEach((el) => {
				const day = dayjs(el.startTime, 'YYYY-MM-DDTHH:mm:ss.SSSZ').format('DD MMMM');
				const startTime = dayjs(el.startTime, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
				const endTime = dayjs(el.endTime, 'YYYY-MM-DDTHH:mm:ss.SSSZ');

				if (!reports[day]) {
					reports[day] = {
						reports: undefined,
						total: undefined,
					};
					reports[day].reports = [{
						startTime,
						endTime,
						id: el.id,
					}];
					reports[day].total = endTime
						.subtract(startTime.hour(), 'hour')
						.subtract(startTime.minute(), 'minute')
						.subtract(startTime.second(), 'second');
				} else {
					reports[day].reports.unshift({
						startTime,
						endTime,
						id: el.id,
					});
					reports[day].total = endTime
						.subtract(startTime.hour(), 'hour')
						.subtract(startTime.minute(), 'minute')
						.subtract(startTime.second(), 'second')
						.add(reports[day].total.hour(), 'hour')
						.add(reports[day].total.minute(), 'minute')
						.add(reports[day].total.second(), 'second');
				}
			});
			dispatch(fetchSuccess(reports));
		} catch (err) {
			dispatch(fetchFail(err));
		}
	};
}
