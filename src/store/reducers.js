// reducers.js
import { combineReducers } from 'redux-immutable';
import timerReducer from './timer/TimerReducer';
import reportsReducer from './reports/ReportsReducer';

const createRootReducer = () => combineReducers({
	timerReducer,
	reportsReducer,
});
export default createRootReducer;
