import { compose, applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import createRootReducer from './reducers';

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const store = createStore(createRootReducer(), composeEnhancers(
	applyMiddleware(thunk)
));
export default store;
