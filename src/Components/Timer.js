import React from 'react';
import styled from 'styled-components';
import { TimeDisplay } from './TimeDisplay';

const TimerWrapper = styled.View`
	margin-top: 20;
	flex-grow: 0.25;
`;

export function Timer({ time }) {
	return (
		<TimerWrapper>
			<TimeDisplay time={time} />
		</TimerWrapper>
	);
}
