import React from 'react';
import { LogoText, MyText } from './Text';

export default function () {
	return (
		<>
			<LogoText>
				<MyText>
                    TIME
					{' '}
				</MyText>
				<MyText color="white">
                    TRACK.
				</MyText>
			</LogoText>
		</>
	);
}
