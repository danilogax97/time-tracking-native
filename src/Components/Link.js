import { TouchableOpacity } from 'react-native';
import React from 'react';
import styled from 'styled-components';
import { WhiteText, GreenLinkText } from './Text';


const LinkWrapper = styled.View`
  flex-direction: row;
  justify-content: center;
`;
export const LinkTo = ({ navigateTo, text, linkText }) => (
	<LinkWrapper>
		<WhiteText>
			{text}
			{' '}
			{' '}
		</WhiteText>
		<TouchableOpacity onPress={() => navigateTo()}>
			<GreenLinkText>
				{linkText}
			</GreenLinkText>
		</TouchableOpacity>
	</LinkWrapper>
);
