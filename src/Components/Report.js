import styled from 'styled-components';
import React from 'react';
import { Bold, TimeTextLightGray } from './Text';
import { displayDifference } from '../lib/helpers';

const ReportHeader = styled.Text`
	margin-bottom: 10;
`;
const ReportWrapper = styled.View`
	margin-bottom: 50;
`;
const ReportItem = styled.View`
	align-items: stretch;
	flex:1;
	flex-direction: row;
	justify-content: space-between;
	border-bottom-width: 0.5;
	border-bottom-color: gray;
`;
const ReportItemTotal = styled(ReportItem)`
	border-top-color: #20c284;
	border-top-width: 1;
	border-bottom-width: 0;
`;
const TotalItemText = styled.Text`
	line-height: 30;
`;
const ReportItemText = styled(TotalItemText)`
	color: gray;
	line-height: 30;
`;
export const Report = ({ time, total, activities }) => (
	<ReportWrapper>
		<ReportHeader>
			<Bold>
                Time Report for
				{' '}
				{time.format('dddd')}
				{', '}
				{' '}

			</Bold>
			<TimeTextLightGray>
				{time.format('DD MMM')}
                :
			</TimeTextLightGray>
		</ReportHeader>
		{activities && activities.map((item) => (
			<ReportItem
				key={item.id}
			>
				<ReportItemText>
					{item.startTime.format('HH:mm')}
					{' - '}
					{item.endTime.format('HH:mm')}
				</ReportItemText>
				<ReportItemText>{displayDifference(item.startTime, item.endTime)}</ReportItemText>
			</ReportItem>
		))}
		<ReportItemTotal>
			<TotalItemText>
                Total:
			</TotalItemText>
			<TotalItemText>
				{' '}
				{total && total.format('HH:mm')}
			</TotalItemText>
		</ReportItemTotal>

	</ReportWrapper>
);
