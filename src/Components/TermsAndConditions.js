import styled from 'styled-components';
import React from 'react';

const GrayText = styled.Text`
  color: #9d9d9d;
  text-align:center;
`;
const BoldAndUnderline = styled.Text`
  text-decoration: underline;
  text-decoration-color: #9d9d9d;
  font-weight: bold;
`;
const TermsWrapper = styled.View`
  margin-top: 20;
`;
export const TermsAndConditions = () => (
	<TermsWrapper>
		<GrayText>
            By clicking sign up you agree to TIME TRACK's
			<BoldAndUnderline> Terms and conditions </BoldAndUnderline>
            and
			<BoldAndUnderline> Privacy Policy </BoldAndUnderline>
		</GrayText>
	</TermsWrapper>
);
