import styled, { css } from 'styled-components';

const ActiveInput = css`
 border-bottom-color: #20c284;
 font-style: normal;
`;
const ActiveLabel = css`
 color: #20c284;
`;
export const StyledInput = styled.TextInput`
  border-bottom-color: gray;
  background-color: transparent;
  margin-bottom: 20px;
  border-bottom-color: white;
  border-bottom-width: 1;
  color:white;
  font-style: italic;
  line-height: 35;
  ${(props) => props.focused && ActiveInput}
`;
export const Label = styled.Text`
  color: white;
  font-weight: 800;
  ${(props) => props.focused && ActiveLabel}
`;
