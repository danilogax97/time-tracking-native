import styled from 'styled-components';
import React from 'react';

export const ErrorMessage = styled.Text`
  font-weight: 600;
  color: darkred;
  margin-bottom: 20;
`;
export const LogoText = styled.Text`
  color:#20c284;
  font-size: 26;
  font-weight: bold;
  flex:1;
`;
export const MyText = styled.Text`
  ${(props) => (props.color ? `color: ${props.color}` : '')}
`;
export const TextCentered = styled.Text`
  text-align: center;
`;
export const WhiteText = styled(TextCentered)`
  color: white;
`;
export const SignOutLinkText = styled(WhiteText)`
  text-decoration: underline;
  text-decoration-color: gray;
  font-size: 12;
`;
export const GreenLinkText = styled(WhiteText)`
  color: #20c284;
  font-weight: 700;
`;
export const TimeTextLightGray = styled.Text`
  position: absolute;
  top: 100%;
  color: gray;
  font-size: 16;
`;
export const BigText = styled.Text`
	font-size: 60;
	font-weight: 300;
`;
export const Bold = styled.Text`
  font-weight: bold;
`;
export const HeaderText = styled(TextCentered)`
	font-weight: bold;
	font-size: 26;
	margin-bottom: 25;
`;
export const GrayText = styled(TextCentered)`
	color:gray;
`;
export const RedText = styled(TextCentered)`
	color: red;
`;
export const TextLarger = styled(TextCentered)`
	font-size: 16;
`;
