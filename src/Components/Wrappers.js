import styled from 'styled-components';

export const FormWrapper = styled.View`
  margin-left: 15;
  margin-right: 15;
  align-self: stretch;
  flex:3.5;
`;
export const BaseWrapper = styled.KeyboardAvoidingView`
  padding-left: 5;
  padding-right: 5;
  padding-top: 20%;
  background-color: #202630;
  flex-direction: column;
  align-items: center;
  flex:1;
  justify-content: center;
  
`;
export const RelativeWrapper = styled.View`
	
`;
export const ScrollViewWrapper = styled.ScrollView`
	padding-left: 20;
	padding-right: 20;
	margin-top: 5%;
	flex:16;
	align-self: stretch;
`;
