import React from 'react';
import styled from 'styled-components';
import { BigText, TimeTextLightGray } from './Text';

const TimeDisplayFlexBox = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
export const Time = styled.View`
 font-size: 62px;
 position: relative;
`;
export const TimeDisplay = ({ time }) => (
	<TimeDisplayFlexBox>
		<Time>
			<BigText>
				{`${time.format('HH')} : ` }
			</BigText>
		</Time>

		<Time>
			<BigText>
				{time.format('ss') }
			</BigText>
		</Time>
		<TimeTextLightGray>
			Hours
			{'           '}
			Minutes
		</TimeTextLightGray>
	</TimeDisplayFlexBox>
);
