import dayjs from 'dayjs';
import React from 'react';
import styled from 'styled-components';
import { TextCentered } from './Text';

export const CurrentDateDisplay = () => {
	const day = dayjs();
	return (
		<CurrentDay>
			<TextCentered>
				{day.format('dddd')}
				{' '}
				{' '}
				<LightText>
					{day.format('DD MMM, HH:mm')}
				</LightText>
			</TextCentered>
		</CurrentDay>
	);
};
const CurrentDay = styled.View`
	align-self: flex-start;
	margin-bottom: 35;
`;
const LightText = styled.Text`
	font-weight: 200;
	color:gray;
`;
