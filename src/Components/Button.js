import styled from 'styled-components';
import { View } from 'react-native';
import React from 'react';

const ButtonWrapper = styled.TouchableHighlight`
	justify-content: center;
	align-items: center;
	background-color: ${(props) => props.color || '#20c284'};
	border-radius: 8;
	height: 35px;
	margin-bottom: 15;
`;
const ButtonText = styled.Text`
	color: ${(props) => props.color || 'white'};;
	font-weight: 600;
`;

export function Button({onPress, text, color, textColor,}) {
	return (
		<ButtonWrapper onPress={onPress} color={color}>
			<View>
				<ButtonText color={textColor}>
					{text}
				</ButtonText>
			</View>
		</ButtonWrapper>
	);
}
