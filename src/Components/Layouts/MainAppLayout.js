import React from 'react';
import styled from 'styled-components';
import { AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { SignOutLinkText } from '../Text';
import Api from '../../lib/api';

const Header = styled.View`
  padding-left: 10;
  padding-right: 10;
  padding-top: 50;
  background-color: #1F2630;
  flex: 0.9;
  width: 100%;
  flex-direction: row;
  justify-content: ${(props) => (props.backButton ? 'space-between' : 'flex-end')};
`;
const MainAppWrapper = styled.View`
  flex-direction: column;
  align-items: center;
  flex:1;
  justify-content: center;
`;
const SignOutTouchable = styled.TouchableOpacity`

`;
const GoBackTouchable = styled.TouchableOpacity`

`;
export default function (WrappedComponent) {
	class Wrapper extends WrappedComponent {
		async handleSignOut() {
			const {
				navigation,
				clearTimer,
			} = this.props;
			try {
				await Api.signOut(await AsyncStorage.getItem('accessToken'));
				await AsyncStorage.removeItem('accessToken');
				await AsyncStorage.removeItem('refreshToken');
				clearTimer();
				navigation.navigate('Loader');
			} catch (error) {
				// @TODO handle errorf
				await AsyncStorage.removeItem('accessToken');
				await AsyncStorage.removeItem('refreshToken');
				navigation.navigate('Loader');
			}
		}

		render() {
			const shouldHaveBackButton = !this.props.navigation.isFirstRouteInParent();
			return (
				<MainAppWrapper>
					<Header
						backButton={shouldHaveBackButton}
					>
						{shouldHaveBackButton && (
							<GoBackTouchable onPress={() => this.props.navigation.pop()}>
								<SignOutLinkText>
                                    GO BACK
								</SignOutLinkText>
							</GoBackTouchable>
						)}

						<SignOutTouchable onPress={() => this.handleSignOut()}>
							<SignOutLinkText>
                             SIGN OUT
							</SignOutLinkText>
						</SignOutTouchable>
					</Header>
					{super.render()}
				</MainAppWrapper>
			);
		}
	}

	const mapDispatchToProps = (dispatch) => ({
		clearTimer: dispatch({ type: 'CLEAR_TIMER' }),
	});
	return connect(null, mapDispatchToProps)(Wrapper);
}
