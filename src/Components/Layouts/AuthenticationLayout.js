import React from 'react';
import styled from 'styled-components';
import { Keyboard, Platform } from 'react-native';
import Logo from '../LogoText';
import { BaseWrapper } from '../Wrappers';

const LogoImage = styled.Image`
  width: 200;
  flex: 0.8;
`;

export default function (WrappedComponent) {
	return class extends React.Component {
		constructor(props) {
			super(props);
			this.state = {
				keyboardActive: false,
			};
		}

		componentDidMount() {
			this.keyboardDidShowListener = Keyboard.addListener(
				// Platform.select({
				// 	ios: () => 'keyboardWillShow',
				// 	android: () => 'keyboardDidShow',
				// })(),
				Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
				this.keyboardDidShow,
			);
			this.keyboardDidHideListener = Keyboard.addListener(
				'keyboardWillHide',
				this.keyboardDidHide,
			);
		}

		componentWillUnmount() {
			this.keyboardDidShowListener.remove();
			this.keyboardDidHideListener.remove();
		}

		keyboardDidShow = () => {
			this.setState({ keyboardActive: true });
		}

		keyboardDidHide = () => {
			this.setState({ keyboardActive: false });
		}


		render() {
			return (
				<BaseWrapper behavior="padding">
					{/*{!this.state.keyboardActive && <LogoImage source={require('../../../assets/logo.png')} />}*/}
					{/*{!this.state.keyboardActive && <Logo />}*/}
					{<LogoImage source={require('../../../assets/logo.png')} />}
					{<Logo />}
					<WrappedComponent {...this.props} />
				</BaseWrapper>
			);
		}
	};
}
