import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import TimeTracking from './src/screens/TimeTracking';
import AuthStack from './src/screens/Authentication';
import Loader from './src/screens/Loader';
import store from './src/store';

const AppContainer = createAppContainer(
	createSwitchNavigator({
		Loader,
		Auth: AuthStack,
		TimeTracking,
	}, {
		initialRouteName: 'Loader',
	})
);

export default function () {
	return (
		<Provider store={store}>
			<AppContainer />
		</Provider>
	);
}
